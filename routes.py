from app import app
from flask import render_template

from EventMgrAPI.RoomService import RoomService

@app.route('/test')
def test():
	return "Test"
	
@app.route('/')
@app.route('/index')
def home():
	return render_template('home.html', title='Home')

@app.route('/room_service_list')
def room_service_list():
	all_request_ids = RoomService.get_total_requests()
	
	requests = []
	for request_id in all_request_ids["id_list"]:
		requests.append(RoomService(request_id))
		
	return render_template("service_requests_table.html", requests=requests, title="View Requests")
	

		
	
